
## Server
### Install virtualenv
```bash
pip install virtualenv
```
### Create a virtualenv
```bash
virtualenv simple-rest-api -p python3
```
### Install Flask
```bash
pip install Flask
```
### Main app
```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return 'Server Works!'

@app.route('/greet')
def say_hello():
    return 'Hello from Server'
```
### Environment variables

#### Linux/Mac
```bash
export FLASK_ENV=development
export FLASK_APP=api.py
```
#### Windows
```bash
set FLASK_ENV="development"
set FLASK_APP="api.py"
```
### Run API
```bash
flask run
```
or for external requests
```bash
flask run --host=0.0.0.0
```
## Client

### Install requests
```bash
pip install requests
```
### Run client
```bash
python client.py
```