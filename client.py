import requests

# https://realpython.com/python-requests/

# /students
response_students = requests.get('http://localhost:5000/students')
print(type(response_students))
print(response_students)
print(response_students.headers)
print(response_students.text)
print(response_students.json())

data1 = response_students.json()

for e in data1['students']:
    print(f"ID = {e['id']}\tNAME = {e['name']}")

# Student
response_student = requests.get('http://localhost:5000/student')
data2 = response_student.json()
name = data2['name']
active = data2['active']
note = data2['note']
print(f'The user {name} has a note = {note}')

# Request an endpoint with different method
response_get = requests.get('http://localhost:5000/login')
print(response_get.text)
response_post = requests.post('http://localhost:5000/login')
print(response_post.text)
