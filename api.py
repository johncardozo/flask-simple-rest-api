from flask import Flask, request, render_template, jsonify

app = Flask(__name__, template_folder='templates')


@app.route('/')
def index():
    return 'Index Page'


@app.route('/hello')
def hello():
    return 'Hello, greetings from different endpoint'

# adding variables
@app.route('/user/<username>')
def show_user(username):
    return render_template("user-profile.html", username=username)


@app.route('/post/<int:post_id>')
def show_post(post_id):
    # returns the post, the post_id should be an int
    return str(post_id)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # check user details from db
        return login_user()
    elif request.method == 'GET':
        # serve login page
        return serve_login_page()

def login_user():
    return 'USER LOGGED!!!'

def serve_login_page():
    return 'form for login'

@app.route('/student')
def student():
    data = {
        'name': 'john',
        'active': True,
        'note': 100
    }
    return jsonify(data)


@app.route('/students')
def students():
    data = {
        "students": [
            {'id': 1, 'name': 'hugo'},
            {'id': 2, 'name': 'paco'},
            {'id': 3, 'name': 'luis'}
        ]
    }
    return jsonify(data)
